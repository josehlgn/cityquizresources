package cat.itb.cityquiz.presentation.screens.results;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.start.MainViewModel;

public class ResultsFragment extends Fragment {


    @BindView(R.id.textView2)
    TextView textView2;
    private MainViewModel mViewModel;

    public static ResultsFragment newInstance() {
        return new ResultsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.results_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        mViewModel.getGameMutableLiveData().observe(this, this::onGameChanged);
        display();
    }

    private void onGameChanged(Game game) {
        if (game!=null && !game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.go_again_to_quiz);
        }
    }

    private void display() {
        textView2.setText(String.valueOf(mViewModel.getGameMutableLiveData().getValue().getNumCorrectAnswers()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.playAgain)
    public void onViewClicked() {
        mViewModel.createGame();
    }
}
