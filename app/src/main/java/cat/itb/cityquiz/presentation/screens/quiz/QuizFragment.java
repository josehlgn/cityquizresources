package cat.itb.cityquiz.presentation.screens.quiz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.presentation.screens.start.MainViewModel;

public class QuizFragment extends Fragment {

    @BindView(R.id.imageViewer)
    ImageView imageViewer;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    private MainViewModel mViewModel;
    Question question;

    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        // TODO: Use the ViewModel
        MutableLiveData<Game> mutableLiveData = mViewModel.getGameMutableLiveData();
        mutableLiveData.observe(this, this::onGameChanged);


    }

    private void onGameChanged(Game game) {
        if (!game.isFinished()){
            display(game);
        } else {
            Navigation.findNavController(getView()).navigate(R.id.go_to_results);
        }
    }

    private void display(Game game) {
        if (!game.isFinished()) {
            question = game.getCurrentQuestion();
            List<City> cities = question.getPossibleCities();
            City city = question.getCorrectCity();

            String fileName = ImagesDownloader.scapeName(city.getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            imageViewer.setImageResource(resId);

            button.setText(cities.get(0).getName());
            button2.setText(cities.get(1).getName());
            button3.setText(cities.get(2).getName());
            button4.setText(cities.get(3).getName());
            button5.setText(cities.get(4).getName());
            button6.setText(cities.get(5).getName());
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    public void questionAnswered(int answer){
        mViewModel.questionAnswered(answer);
    }

    @OnClick({R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6})
    public void onViewClicked(View view) {
        switch (view.getId()){
            case R.id.button:
                questionAnswered(0);
                break;
            case R.id.button2:
                questionAnswered(1);
                break;
            case R.id.button3:
                questionAnswered(2);
                break;
            case R.id.button4:
                questionAnswered(3);
                break;
            case R.id.button5:
                questionAnswered(4);
                break;
            case R.id.button6:
                questionAnswered(5);
                break;
        }
    }
}
